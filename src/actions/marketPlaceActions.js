import axios from "axios";
import { GET_HMO } from "./types";

export const getHmo = () => dispatch => {
  axios.get("/hmo").then(res => {
    dispatch({
      type: GET_HMO,
      payload: res.data
    });
  });
};
