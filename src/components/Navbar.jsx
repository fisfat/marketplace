import React, { Component } from "react";

class Navbar extends Component {
  render() {
    return (
      <header className="navbar navbar-fixed">
        <div className="navbar--header">
          <a href="index.html" className="logo">
            <img src="assets/img/logo.png" style={{ width: "60%" }} alt="" />
          </a>
          <a
            href="#"
            className="navbar--btn"
            data-toggle="sidebar"
            title="Toggle Sidebar"
          >
            <i className="fa fa-bars" />
          </a>
        </div>
        <div className="navbar--search">
          <form action="search-results.html">
            <input
              type="search"
              name="search"
              style={{ width: "100%" }}
              className="form-control"
              placeholder="search.."
              required
            />
            <button className="btn-link">
              <i className="fa fa-search" style={{ width: "100%" }} />
            </button>
          </form>
        </div>

        <div className="navbar--nav ml-auto">
          <ul className="nav">
            <li className="nav-item">
              <a href="#" className="nav-link">
                <i className="fa fa-home" />
                <span className="badge text-white bg-blue">5</span>
              </a>
            </li>
            <li className="nav-item">
              <a href="#" className="nav-link">
                <i className="fa fa-bell" />
                <span className="badge text-white bg-blue">7</span>
              </a>
            </li>

            <li className="nav-item">
              <a href="mailbox_inbox.html" className="nav-link">
                <i className="fa fa-comments" />
                <span className="badge text-white bg-blue">4</span>
              </a>
            </li>
            <li className="nav-item">
              <a href="mailbox_inbox.html" className="nav-link">
                <i className="fa fa-shopping-cart" />
                <span className="badge text-white bg-blue">1</span>
              </a>
            </li>
          </ul>
        </div>
      </header>
    );
  }
}
export default Navbar;
