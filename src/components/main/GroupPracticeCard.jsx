import React, { Component } from "react";

export default class GroupPracticeCard extends Component {
  render() {
    const { state, city, streetName, description, regFee } = this.props.data;
    const click = this.props.click;
    const address = state + ", " + city + ", " + streetName;
    return (
      <div className="col-md-4">
        <div className="panel" style={{ borderRadius: 20 + "px" }}>
          <div className="miniStats--panel">
            <div className="miniStats--header bg-p">
              <img
                src="assets/img/dashboard.png"
                style={{ width: "auto", height: "auto" }}
              />
            </div>

            <div className="miniStats--body panel-content pt-12">
              <h5 className="miniStats--title">{description}</h5>

              <div className="form-group row">
                <div className="col-md-5">
                  <p className="miniStats--caption">
                    <i className="fa fa-calendar" /> 08/08/ 2019
                  </p>
                </div>
                <div className="col-md-4">
                  <p className="miniStats--caption">
                    <i className="fa fa-clock" /> 6pm
                  </p>
                </div>
                <div className="col-md-12">
                  <p className="miniStats--caption">
                    <i className="fa fa-map-marker" /> {address}
                  </p>
                </div>
              </div>
              <div className="col-md-12">
                <a
                  href="#vCenteredModal"
                  className="btn btn-info"
                  style={{ width: "100%" }}
                  data-toggle="modal"
                  //   onClick={() => click(_id)}
                >
                  Explore
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
