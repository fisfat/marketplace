import React from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import SidebarNav from "./components/SidebarNav";
import Main from "./components/Main";
import Test from "./components/Test";
import HmoBody from "./components/main/HmoBody";
import GroupPracticeBody from "./components/main/GroupPracticeBody";
import setAuthTokenAndBaseURL from "./utils/setAuthToken";
import jwt_decode from "jwt-decode";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import store from "./store";
import { Provider } from "react-redux";

import { getUser } from "./actions/userAction";

// if (localStorage.token) {
//   console.log("token eists");
const token = localStorage.getItem("token");
setAuthTokenAndBaseURL(token);

store.dispatch(getUser(jwt_decode(token)));

// const decoded = jwt_decode(localStorage.jwtToken);

// store.dispatch(setCurrentUser(decoded));

// const currentTime = Date.now() / 1000;

// if (decoded.exp < currentTime) {
//   // window.location.href("/login");
//   store.dispatch(logoutUser());
//   store.dispatch(clearCurrentProfile());
//   // this.props.history.push("/login");
// }
// } else {
//   console.log("we have no token");
// }

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Navbar />
        <SidebarNav />
        <Main />
        <div className="App">
          <Switch>
            <Route exact path="/hmo" component={HmoBody} />
            <Route exact path="/group-practice" component={GroupPracticeBody} />
            <Route exact path="/signup" component={Test} />
            <Route exact path="/verify" component={Test} />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
